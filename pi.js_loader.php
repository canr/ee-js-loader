<?php if ( !defined ('BASEPATH')) exit ('No direct script access allowed');

// author: Dennis Bond, Michigan State University (bonddenn@msu.edu)
// February 3, 2010
// Renamed June 23, 2014 for responsive reference design

$plugin_info = array (
    'pi_name'           => 'JavaScript Loader',
    'pi_version'        => '1.1.1',
    'pi_author'         => 'Dennis Bond',
    'pi_description'    => 'Provides the jQuery script tags and document.ready function'
);

class Js_loader
{
    // need this because constructors cannot return data
    var $return_data = "";

    function Js_loader()
    {
        // get an instance of the ExpressionEngine object
        $this->EE =& get_instance();
        
        // fetch data
        $document_ready_code = $this->EE->TMPL->tagdata;
        $plugins_list = $this->EE->TMPL->fetch_param('plugins');
        $version = $this->EE->TMPL->fetch_param('version', '1.11.3'); // jQuery library version
        $anr_bar = $this->EE->TMPL->fetch_param('anr_bar');
        $mobile_menu = $this->EE->TMPL->fetch_param('mobile_menu');
        $load_jquery = $this->EE->TMPL->fetch_param('load_jquery');
        $protocol = $_SERVER['HTTPS'] ? 'https:' : 'http:';
        
        // break the plugins list into an array
        $plugins = explode(",", $plugins_list);
        
        // generate the jquery code
        $jquery_code = "";
        if ($load_jquery!="no")
        {
            $jquery_code .= "<script type=\"text/javascript\" src=\"{$protocol}//ajax.googleapis.com/ajax/libs/jquery/{$version}/jquery.min.js\"></script>\n";
            /*$jquery_code .= "<script type=\"text/javascript\" src=\"{$protocol}//expeng.anr.msu.edu/javascript/foundation.js\"></script>\n"; */
            $jquery_code .= "<!--[if IE 8]><script type=\"text/javascript\" src=\"{$protocol}//expeng.anr.msu.edu/javascript/respond.min.js\"></script><![endif]-->\n";
        }
        if ($anr_bar=="yes")
        {
            $jquery_code .= "<script type=\"text/javascript\" src=\"{$protocol}//expeng.anr.msu.edu/anr_bar/network_element.js\"></script>\n";
        }
        if ($mobile_menu=="yes")
        {
            $jquery_code .= "<script type=\"text/javascript\" src=\"{$protocol}//expeng.anr.msu.edu/mobile_menu/mobile_menu.js\"></script>\n";
        }
        if ($plugins_list)
        {
            foreach ($plugins as $plugin)
            {
                $jquery_code .= "<script type=\"text/javascript\" src=\"{$protocol}//expeng.anr.msu.edu/javascript/".$plugin.".js\"></script>\n";
            }
        }

        $jquery_code .= "<script type=\"text/javascript\">\n"
        ."//<!-- hide javascript from validator\n"
        ."    $(document).ready(function(){"
        .$document_ready_code
        ."    });\n"
        ."// end hiding from validator -->\n"
        ."</script>";

        $this->return_data = $jquery_code;
    }
}
?>
